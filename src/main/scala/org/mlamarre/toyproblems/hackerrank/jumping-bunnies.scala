package org.mlamarre.toyproblems.hackerrank.jumpingbunnies

// https://www.hackerrank.com/challenges/jumping-bunnies/problem

object Solution {

  import scala.annotation.tailrec

  @tailrec
  def primeFactors(n: Long, nextOdd: Long = 3L, accum: List[Long] = Nil): List[Long] = {
    if (n < 2) accum.reverse
    else if (n % 2 == 0) primeFactors(n / 2, nextOdd, 2 :: accum)
    else if (n % nextOdd == 0) primeFactors(n / nextOdd, nextOdd, nextOdd :: accum)
    else primeFactors(n, nextOdd + 2, accum)
  }

  def main(args: Array[String]) = {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution
     */

    import scala.io.StdIn.readLine

    val numBuns: Int = readLine().toInt
    val jumpLengths: List[Long] = readLine().split(" ").toList.map(_.toLong)

    val allFactors: List[List[Long]] = jumpLengths.map(l => primeFactors(l))
    val result: List[Long] =
      allFactors.foldLeft(allFactors.head)((accum, factorsN) => (factorsN diff accum) ::: accum)
    println(s"${result.foldLeft(1L)((accum, n) => accum * n)}")
  }
}
