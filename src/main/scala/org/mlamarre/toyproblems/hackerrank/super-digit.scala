package org.mlamarre.toyproblems.hackerrank.superdigit

// https://www.hackerrank.com/challenges/super-digit/problem

object Solution {

  // Originally I did n.toList.map(_.asDigit.toLong). That is a LOT of Long values if n is a
  // lengthy string.
  // def sumDigits(n: String): Long = n.toList.map(_.asDigit).sum.toLong

  // This is actually significantly more efficient in terms of both speed and memory usage.
  def sumDigitsBetter(n: String): Long = n.toList.foldLeft(0L)((acc, c) => acc + c.asDigit)

  // Given the range defined for n, and the fact that is could be repeated 10^5 times via k, I
  // chose to leave n as a string.
  def superDigit(n: String, k: Int): Long = {
    @scala.annotation.tailrec
    def loopR(s: String): Long = {
      sumDigitsBetter(s) match {
        case x if x / 10 == 0 => x
        case y => loopR(y.toString)
      }
    }

    // I originally did loopR(n * k), but that can result in a HUGE string if n and k are both very
    // large. Particularly combined with my original sumDigits() implementation. So we sum the
    // digits of n once, then multiply that value by k before we start the iteration.
    loopR((sumDigitsBetter(n) * k).toString)
  }

  def main(args: Array[String]) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution
*/
    import scala.io.StdIn.readLine

    val input = readLine()
    val (n, k) = input.splitAt(input.indexOf(" "))
    println(superDigit(n, k.trim.toInt))
  }

  // I had an idea for another implementation that seemed like it worked when I did some manual
  // test cases. I figured I would implement it to a) see if it generates the same results as the
  // function above and b) see which one is more efficient.
  def superDigit2(n: String, k: Int): Long = ???
}
