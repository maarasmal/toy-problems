package org.mlamarre.toyproblems.hackerrank.pwcracker

// https://www.hackerrank.com/challenges/password-cracker-fp/problem

object Solution {

    final val WrongPassword = "WRONG PASSWORD"

    def processAttempt(passwords: List[String], attempt: String): String = {

      @scala.annotation.tailrec
      def loopR(pwds: List[String], att: String, acc: List[String]): String = {
        val regex = s"${pwds.head}(.*)".r
        att match {
          case regex(theRest) if theRest.isEmpty() => (pwds.head :: acc).reverse.mkString(" ")
          case regex(theRest) => loopR(passwords, theRest, pwds.head :: acc)
          case _ => if (pwds.tail.isEmpty) WrongPassword else loopR(pwds.tail, att, acc)
        }
      }

      loopR(passwords, attempt, Nil)
    }

    def main(args: Array[String]) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution
*/
      import scala.io.StdIn.readLine

      val numCases = readLine().toInt
      (1 to numCases).foreach{ i =>
        val numPasswords = readLine().toInt
        val passwords = readLine().split(" ").toList
        val loginAttempt = readLine()
        println(processAttempt(passwords, loginAttempt))
      }
    }
}