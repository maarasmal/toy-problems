package org.mlamarre.toyproblems
package adventofcode
package day2part1

/**
 * Models a shape chosen when playing a game of rock, paper, scissors as described here:
 * https://adventofcode.com/2022/day/2
 */
sealed trait Shape {
  def beats(other: Shape): Boolean
  def score: Int
}

object Shape {
  case object Rock extends Shape {
    def beats(other: Shape) = other == Scissors
    val score = 1
  }
  case object Paper extends Shape {
    def beats(other: Shape) = other == Rock
    val score = 2
  }
  case object Scissors extends Shape {
    def beats(other: Shape) = other == Paper
    val score = 3
  }

  def fromString(s: String): Option[Shape] =
    s.toLowerCase() match {
      case "a" | "x" => Some(Rock)
      case "b" | "y" => Some(Paper)
      case "c" | "z" => Some(Scissors)
      case _         => None
    }
}

object RockPaperScissors {

  def calculateTotalScore(rounds: List[(Shape, Shape)]): Int =
    rounds.foldLeft(0) { case (scoreAcc, (oppItem, myItem)) =>
      scoreAcc + myItem.score + scoreOutcome(myItem, oppItem)
    }

  private def scoreOutcome(myItem: Shape, oppItem: Shape): Int = {
    if (myItem == oppItem) 3
    else if (myItem.beats(oppItem)) 6
    else 0
  }

}
