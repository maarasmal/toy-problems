package org.mlamarre.toyproblems
package adventofcode
package day2part2

/**
 * Models a shape chosen when playing a game of rock, paper, scissors as described here:
 * https://adventofcode.com/2022/day/2
 */
sealed trait Shape {
  def beats: Shape
  def losesTo: Shape
  def score: Int
}

object Shape {
  case object Rock extends Shape {
    override def beats: Shape = Scissors
    override def losesTo: Shape = Paper
    val score = 1
  }
  case object Paper extends Shape {
    override def beats: Shape = Rock
    override def losesTo: Shape = Scissors
    val score = 2
  }
  case object Scissors extends Shape {
    override def beats: Shape = Paper
    override def losesTo: Shape = Rock
    val score = 3
  }

  def fromString(s: String): Option[Shape] =
    s.toLowerCase() match {
      case "a" => Some(Rock)
      case "b" => Some(Paper)
      case "c" => Some(Scissors)
      case _   => None
    }
}

sealed trait Outcome
object Outcome {
  case object Lose extends Outcome
  case object Tie extends Outcome
  case object Win extends Outcome

  def fromString(s: String): Option[Outcome] =
    s.toLowerCase() match {
      case "x" => Some(Lose)
      case "y" => Some(Tie)
      case "z" => Some(Win)
      case _   => None
    }
}

object RockPaperScissors {

  def calculateTotalScore(rounds: List[(Shape, Outcome)]): Int =
    rounds.foldLeft(0) { case (scoreAcc, (oppItem, desiredOutcome)) =>
      val myItem = chooseMyItem(oppItem, desiredOutcome)
      scoreAcc + myItem.score + scoreOutcome(myItem, oppItem)
    }

  private def scoreOutcome(myItem: Shape, oppItem: Shape): Int = {
    if (myItem == oppItem) 3
    else if (myItem.beats == oppItem) 6
    else 0
  }

  private def chooseMyItem(oppItem: Shape, desiredOutcome: Outcome): Shape =
    desiredOutcome match {
      case Outcome.Lose => oppItem.beats
      case Outcome.Tie  => oppItem
      case Outcome.Win  => oppItem.losesTo
    }

}
