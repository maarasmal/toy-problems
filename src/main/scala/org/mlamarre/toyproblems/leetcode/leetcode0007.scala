package org.mlamarre.toyproblems.leetcode

/**
 * Given a 32-bit integer x, return an integer value that is x with its digits reversed. If this
 * operation would cause over/under-flow, return 0 instead.
 *
 * https://leetcode.com/problems/reverse-integer/ Difficulty: Easy
 */
package leetcode0007 {

  // Note that the problem statement says to assume that the environment does not allow you to store
  // 64-bit integers, thus the solution cannot use things like Long and BigInt.
  object Solution {
    def reverse(x: Int): Int = {
      @scala.annotation.tailrec
      def loopR(r: Int, acc: Int): Int = {
        if (r == 0) acc
        else {
          val d = r % 10

          // Check if multiplying our accumulator by 10 would cause over/under-flow, or if the
          // multiplication would be safe, but the addition of the current digit would blow up. Per
          // the problem description, we return 0 then. Otherwise, update the accumulator and
          // continue looping through the remaining digits.
          if (acc > Int.MaxValue / 10 || (acc == (Int.MaxValue / 10) && d > 7)) 0
          else if (acc < Int.MinValue / 10 || (acc == (Int.MinValue / 10) && d < -8)) 0
          else loopR(r / 10, (acc * 10) + d)
        }
      }

      loopR(x, 0)
    }
  }

}
