package org.mlamarre.toyproblems.leetcode

package leetcode0118 {

  /**
   * https://leetcode.com/problems/pascals-triangle/
   */
  object Solution {
    def generate(numRows: Int): List[List[Int]] = {

      @scala.annotation.tailrec
      def genRow(parent: List[Int], acc: List[Int]): List[Int] = {
        parent match {
          case Nil              => acc
          case h :: Nil         => 1 :: acc
          case h1 :: h2 :: tail => genRow(h2 :: tail, (h1 + h2) :: acc)
        }
      }

      def genTriangle(n: Int, acc: List[List[Int]]): List[List[Int]] =
        if (n == numRows) genRow(acc.head, List(1)) :: acc
        else genTriangle(n + 1, genRow(acc.head, List(1)) :: acc)

      if (numRows == 1) List(List(1))
      else genTriangle(2, List(List(1))).reverse
    }
  }

}
