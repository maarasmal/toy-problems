package org.mlamarre.toyproblems.leetcode

package leetcode0009 {

  // https://leetcode.com/problems/palindrome-number/
  object Solution {
    def isPalindrome(x: Int): Boolean = {
      x.toString == x.toString.reverse
    }
  }
}
