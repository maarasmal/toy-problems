package org.mlamarre.toyproblems.leetcode

import munit.FunSuite

class Leetcode0009Test extends FunSuite {

  // https://leetcode.com/problems/palindrome-number/
  test("9. Palindrome Number") {
    import leetcode0009.Solution._

    assertEquals(isPalindrome(123), false)
    assertEquals(isPalindrome(112211), true)
    assertEquals(isPalindrome(121), true)
    assertEquals(isPalindrome(-121), false)
    assertEquals(isPalindrome(10), false)
    assertEquals(isPalindrome(-101), false)
    assertEquals(isPalindrome(0), true)
    assertEquals(isPalindrome(4), true)

  }
}
