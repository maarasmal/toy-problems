package org.mlamarre.toyproblems.leetcode

import munit.FunSuite

/** https://leetcode.com/problems/reverse-integer/
  */
class Leetcode0007Test extends FunSuite {

  test("7. Reverse Integer") {
    import leetcode0007.Solution._

    assertEquals(reverse(0), 0)
    assertEquals(reverse(123), 321)
    assertEquals(reverse(-123), -321)
    assertEquals(reverse(120), 21)
    assertEquals(reverse(20), 2)
    assertEquals(reverse(Int.MaxValue), 0)
    assertEquals(reverse(Int.MinValue), 0)
    assertEquals(reverse(Int.MaxValue - 6), 1463847412)
    assertEquals(reverse(Int.MaxValue - 8), 0)
  }

}
