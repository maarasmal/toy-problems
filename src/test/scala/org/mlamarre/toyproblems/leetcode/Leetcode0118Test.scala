package org.mlamarre.toyproblems.leetcode

import munit.FunSuite

/**
 * https://leetcode.com/problems/pascals-triangle/
 */
class Leetcode0118Test extends FunSuite {

  test("118. Pascal's Triangle") {
    import leetcode0118.Solution._

    assertEquals(generate(1), List(List(1)))
    assertEquals(generate(2), List(List(1), List(1, 1)))
    assertEquals(generate(3), List(List(1), List(1, 1), List(1, 2, 1)))
    assertEquals(generate(4), List(List(1), List(1, 1), List(1, 2, 1), List(1, 3, 3, 1)))
    assertEquals(
      generate(5),
      List(List(1), List(1, 1), List(1, 2, 1), List(1, 3, 3, 1), List(1, 4, 6, 4, 1))
    )
    assertEquals(
      generate(6),
      List(
        List(1),
        List(1, 1),
        List(1, 2, 1),
        List(1, 3, 3, 1),
        List(1, 4, 6, 4, 1),
        List(1, 5, 10, 10, 5, 1)
      )
    )
    assertEquals(
      generate(7),
      List(
        List(1),
        List(1, 1),
        List(1, 2, 1),
        List(1, 3, 3, 1),
        List(1, 4, 6, 4, 1),
        List(1, 5, 10, 10, 5, 1),
        List(1, 6, 15, 20, 15, 6, 1)
      )
    )
  }

}
