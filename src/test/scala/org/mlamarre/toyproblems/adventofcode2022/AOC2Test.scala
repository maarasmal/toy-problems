package org.mlamarre.toyproblems
package adventofcode

import cats.effect._
import cats.effect.unsafe.implicits.global
import fs2.{Stream, text}
import fs2.io.file.{Files, Path}
import munit.FunSuite

class AOC2Test extends FunSuite {

  test("Day 2, Part 1 - Basic test using inputs from the problem statement") {
    import day2part1._
    import day2part1.Shape._

    val sampleInputs = List((Rock, Paper), (Paper, Rock), (Scissors, Scissors))
    val totalScore = RockPaperScissors.calculateTotalScore(sampleInputs)

    assertEquals(totalScore, 15)
  }

  test("Day 2, Part 1 - Process input file from AoC site") {
    import day2part1._
    import day2part1.Shape._

    val inputFile = Path(this.getClass.getResource("/adventofcode/aoc2-input.txt").getPath())
    val totalScore = Files[IO]
      .readUtf8Lines(inputFile)
      .filter(!_.trim.isBlank())
      .map { l =>
        l.split(" ").map(Shape.fromString) match {
          case Array(Some(oppChoice), Some(myChoice)) => (oppChoice, myChoice)
          case _ => throw new IllegalArgumentException(s"Invalid input line: $l")
        }
      }
      .compile
      .toList
      .map { allRounds =>
        val totalScore = RockPaperScissors.calculateTotalScore(allRounds)
        println(s"Part 1 - Calculated total score of: $totalScore")
        totalScore
      }
      .unsafeRunSync()

    // This is the answer printed above, and when input into the site, it was judged
    // to be correct. Including it here to make this more of a real test.
    assertEquals(totalScore, 15422)
  }

  test("Day 2, Part 2 - Basic test using inputs from the problem statement") {
    import day2part2._
    import day2part2.Shape._
    import day2part2.Outcome._

    val sampleInputs = List((Rock, Tie), (Paper, Lose), (Scissors, Win))
    val totalScore = RockPaperScissors.calculateTotalScore(sampleInputs)

    assertEquals(totalScore, 12)
  }

  test("Day 2, Part 1 - Process input file from AoC site") {
    import day2part2._
    import day2part2.Shape._

    val inputFile = Path(this.getClass.getResource("/adventofcode/aoc2-input.txt").getPath())
    val totalScore = Files[IO]
      .readUtf8Lines(inputFile)
      .filter(!_.trim.isBlank())
      .map { l =>
        l.split(" ") match {
          case Array(oppChoiceStr, desiredOutcomeStr) =>
            (
              Shape.fromString(oppChoiceStr),
              day2part2.Outcome.fromString(desiredOutcomeStr)
            ) match {
              case (Some(s), Some(o)) => (s, o)
              case _ => throw new IllegalArgumentException(s"Invalid shape or outcome: $l")
            }
          case _ => throw new IllegalArgumentException(s"Invalid input line: $l")
        }
      }
      .compile
      .toList
      .map { allRounds =>
        val totalScore = RockPaperScissors.calculateTotalScore(allRounds)
        println(s"Part 2 - Calculated total score of: $totalScore")
        totalScore
      }
      .unsafeRunSync()

    // This is the answer printed above, and when input into the site, it was judged
    // to be correct. Including it here to make this more of a real test.
    assertEquals(totalScore, 15442)
  }

}
