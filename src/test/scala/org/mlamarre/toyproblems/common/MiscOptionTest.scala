package org.mlamarre.toyproblems
package common

import munit.FunSuite

class MiscOptionTest extends FunSuite {

  case class Status(id: Int, location: String)

  case class Detail(id: Int, name: String)

  // Commentary from me is prefaced with "MAL:" below
  test("Alan's Option exercises") {
    // MAL: I stripped out the println() calls to more clearly see just the actual Option
    // operations.

    val status: Status = Status(1, "US")
    val detailOpt = Option(Detail(1, "One"))

    // Print whole objects where the id's align
    val idsMatch: Option[Detail] = detailOpt.filter(_.id == status.id)

    // Print specific properties using get where the id's align
    // MAL: Potentially unsafe! If detailOpt is a None, filter() will result in a None, and then
    // the get() call will throw an exception
    val nameWhenIdsMatch = detailOpt.filter(_.id == status.id).get.name

    // Print "-" using getOrElse when the id's DO NOT align, notice status.id+1
    // But if I remove the "+1" from the statement below, I get the whole Detail object.
    // How do I get just the name property?
    // MAL: This is safer, but see below for a full description of some unexpected side-effects of
    // the way this is written.
    val nameMaybe = detailOpt.filter(_.id == status.id + 1).getOrElse("-")

    // Very complicated looking getOrElse
    // MAL: These get() calls are again unsafe. In general, I don't think I have ever used get()
    // in non-test code. It should almost never be necessary.
    val detailName =
      if (detailOpt.exists(_.id == status.id)) detailOpt.filter(_.id == status.id).get.name else "-"
    val dash =
      if (detailOpt.exists(_.id == status.id + 1)) detailOpt.filter(_.id == status.id + 1).get.name
      else "-"

    // Using a for comprehension, but it doesn't have the else case
    // MAL: This case is a little unusual, so I left the println() in place. See my test below for
    // more details and a more idiomatic example
    for {
      st <- List(status)
      dd <- detailOpt
      if (st.id == dd.id)
    } println(s"st.id: ${st.id}, st.location: ${st.location}, dd.name = ${dd.name}")
  }

  test("Mike's alternatives") {

    val status: Status = Status(1, "US")
    val detailOpt = Option(Detail(1, "One"))

    // Print whole objects where the id's align
    // MAL: Nothing to change here
    val idsMatch: Option[Detail] = detailOpt.filter(_.id == status.id)

    // Print specific properties using get where the id's align
    // MAL: Notice here that we map and then use getOrElse(). The get() call in the previous test
    // works because detailOpt is a Some, but if it were a None, it would throw an exception.
    // This statement is always safe.
    val nameWhenIdsMatch = detailOpt.filter(_.id == status.id).map(_.name).getOrElse("n/a")

    // Print "-" using getOrElse when the id's DO NOT align, notice status.id+1
    // But if I remove the "+1" from the statement below, I get the whole Detail object.
    // How do I get just the name property?
    // MAL: Note in the test above I left the type off of nameMaybe. That will then compile, but
    // in an IDE you can see that the type of nameMaybe is Serializable, because getOrElse will
    // return either the contents of the Option (in this case, a Detail), or the value provided to
    // getOrElse() (in this case, a String). The type inferencer attempts to find the closest
    // common parent type for those two types, which in this case will be Serializable.
    // If you explicitly declare the type of nameMaybe as String, then the statement in the previous
    // test will generate an error, because a Detail is not a String. So you could add a map() call
    // in there to go Option[Detail] -> Option[String] -> String, like so:
    val nameMaybe: String = detailOpt.filter(_.id == status.id + 1).map(_.name).getOrElse("-")
    // A more type safe way to do this, that doesn't require you to declare the type on nameMaybe2,
    // is to use fold(), which requires that both parameter functions yield the same type. Here,
    // the inferencer will always correctly determine that nameMaybe2 is of type String:
    val nameMaybe2 = detailOpt.filter(_.id == status.id + 1).fold("-")(_.name)

    // Very complicated looking getOrElse
    // MAL: Here the statement structure allows the type inferencer to correctly make detailName and
    // dash both of type String. Note that because of the `if` statement, the `filter` calls are
    // redundant. And either way, they don't provide much safety because you still end up with an
    // Option, and then you call get(), which will throw an exception if detailOpt is a None. In
    // the second statement, it is only "safe" because of the `if` statement. The filter() call
    // provides no safety.
    val detailName =
      if (detailOpt.exists(_.id == status.id)) detailOpt.filter(_.id == status.id).get.name else "-"
    val dash =
      if (detailOpt.exists(_.id == status.id + 1)) detailOpt.filter(_.id == status.id + 1).get.name
      else "-"

    // MAL: So the cleaner way to do this is the same as with nameMaybe above, using either
    // map() + getOrElse(), or fold()
    val detailName2 = detailOpt.filter(_.id == status.id).map(_.name).getOrElse("-")
    val dash2 = detailOpt.filter(_.id == status.id + 1).fold("-")(_.name)

    // Using a for comprehension, but it doesn't have the else case
    // MAL: having a println() at the end of a for comprehension is not a typical use case. More
    // likely what you'd want to do is return the actual name value. Note though that you wrapped
    // status in a List. This works, because there is an implicit conversion available to convert
    // detailOpt into a List[Detail] (which is necessary because all of the generators of a for
    // comprehension must have the same outer container type). But it then means that nameFromFor
    // ends up as a List[String], which is a bit weird.
    val nameFromFor = for {
      st <- List(status)
      dd <- detailOpt
      if (st.id == dd.id)
    } yield dd.name

    // MAL : If you instead wrap status in an Option, then nameFromFor2 ends up an Option[String]
    // here. And at that point, you can easily use getOrElse() in the same ways we have above.
    val nameFromFor2 = for {
      st <- Option(status)
      dd <- detailOpt
      if (st.id == dd.id)
    } yield dd.name
    val nameFromForFinal = nameFromFor2.getOrElse("-")

    // MAL: Here's a new type that composes your two types, but with the detail as an optional
    // value. I did this to demonstrate another common idiom that you'll run into with nested
    // Options.
    case class MyEvent(status: Status, detail: Option[Detail])

    // Create a new MyEvent, wrapped in an Option.
    val e = Option(MyEvent(Status(1, "mike"), Some(Detail(1, "det1"))))

    // Now suppose we want to get the name from the inner Detail, if it exists. From the examples
    // above, you might try calling map(). But there is an option within an option, so you call
    // map() twice. But note the final type of name in this case: nested Options. That is going to
    // be clunky to work with.
    val name: Option[Option[String]] = e.map(_.detail.map(_.name))

    // MAL: Here is where flatMap() comes in. Here, e.flatMap() requires a function of the "shape"
    // MyEvent => Option[B]. But note that the return type of flatMap() is just Option[B]. So the
    // inner map() call results in a Option[String], which ends up being the value returned by the
    // outer flatMap() call.
    val betterName: Option[String] = e.flatMap(_.detail.map(_.name))
  }

  // Sample from Alan:
  // state.detailState
  // .filter(_.details.id === status.id)
  // .fold(makeEmptyGearItem()) { ds =>
  //   ds.details.controllerRef.fold(makeEmptyGearItem()) { cr =>
  //     AnchorLinkPopupMenu.Item(
  //       cr.link.humanized,
  //       Left(new URI(cr.link.uri.toString))
  //     )
  //   }
  // }

  case class Foo(id: Int, name: String)
  case class Bar(id: Int, fooMaybe: Option[Foo])

  test("Get rid of nested fold calls") {
    val bMaybe = Option(Bar(1, Some(Foo(2, "mike"))))
    // bMaybe: Option[Bar] = Some(Bar(1, Some(Foo(2, "mike"))))

    bMaybe.flatMap(_.fooMaybe).fold("empty")(_.name)
    // res20: String = "mike"

    val someNone = Option(Bar(1, None))
    // someNone: Option[Bar] = Some(Bar(1, None))

    someNone.flatMap(_.fooMaybe).fold("empty")(_.name)
    // res22: String = "empty"

    val noneNone = Option.empty[Bar]
    // noneNone: Option[Bar] = None

    noneNone.flatMap(_.fooMaybe).fold("empty")(_.name)
    // res24: String = "empty"

    val result = for {
      b <- bMaybe
      f <- b.fooMaybe
      // do work that needs b
      nm = f.name
    } yield {
      // many many lines
    }
    result.getOrElse("empty")

  }
}
