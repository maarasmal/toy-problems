package org.mlamarre.toyproblems.hackerrank.pwcracker

import munit.FunSuite

class PwCrackerFpTest extends FunSuite {

  test("HR Input 0, test 1") {
    val attempt = "wedowhatwemustbecausewecan"
    val result = Solution.processAttempt(
      "because can do must we what".split(" ").toList,
      attempt
    )
    assertEquals(result.replace(" ", ""), attempt)
  }

  test("HR Input 0, test 2") {
    assertEquals(
      Solution.processAttempt("hello planet".split(" ").toList, "helloworld"),
      Solution.WrongPassword
    )
  }

  test("HR Input 0, test 3") {
    val attempt = "abcd"
    val result = Solution.processAttempt(List("ab", "abcd", "cd"), attempt)
    assertEquals(result.replace(" ", ""), attempt)
  }

  test("HR Input 1, test 1") {
    val attempt = "zfzahm"
    val result = Solution.processAttempt("ozkxyhkcst xvglh hpdnb zfzahm".split(" ").toList, attempt)
    assertEquals(result.replace(" ", ""), attempt)
  }

  test("HR Input 1, test 2") {
    val attempt = "gurwgrb"
    val result =
      Solution.processAttempt("gurwgrb maqz holpkhqx aowypvopu".split(" ").toList, attempt)
    assertEquals(result.replace(" ", ""), attempt)
  }

  test("HR Input 1, test 3") {
    val attempt = "aaaaaaaaaab"
    val result = Solution.processAttempt(
      "a aa aaa aaaa aaaaa aaaaaa aaaaaaa aaaaaaaa aaaaaaaaa aaaaaaaaaa".split(" ").toList,
      attempt
    )
    assertEquals(result, Solution.WrongPassword)
  }

  test("My hand-crafted test 1") {
    val attempt = "zfzahmozkxyhkcst"
    val result = Solution.processAttempt("ozkxyhkcst xvglh hpdnb zfzahm".split(" ").toList, attempt)
    assertEquals(result.replace(" ", ""), attempt)
  }

  test("My hand-crafted test 2") {
    val attempt = "ozkxyzfzahmhkcst"
    val result = Solution.processAttempt("ozkxyhkcst xvglh hpdnb zfzahm".split(" ").toList, attempt)
    assertEquals(result, Solution.WrongPassword)
  }

  test("My hand-crafted test 3") {
    val attempt = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
    val result = Solution.processAttempt(
      "a aa aaa aaaa aaaaa aaaaaa aaaaaaa aaaaaaaa aaaaaaaaa aaaaaaaaaa".split(" ").toList,
      attempt
    )
    assertEquals(result.replace(" ", ""), attempt)
  }

}
