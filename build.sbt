ThisBuild / organization := "org.mlamarre"
ThisBuild / scalaVersion := "2.13.8"

// ============================================================================

// Lines like the above defining `scalaVersion` are called "settings". Settings
// are key/value pairs. In the case of `scalaVersion`, the key is "scalaVersion"
// and the value is "2.13.3"

// It's possible to define many kinds of settings, such as:

name := "toy-problems"
version := "1.0"

// Note, it's not required for you to define these three settings. These are
// mostly only necessary if you intend to publish your library's binaries on a
// place like Sonatype or Bintray.

// Turn on some new Scala 3 compiler options. Significant indentation is the spawn of the devil,
// but the new syntax is probably fine. The "explain" options should give some better messages,
// but we might turn them off at some point.
// scalacOptions ++= Seq(
//   "-explain",
//   "-explain-types",
//   "-new-syntax",
//   "-no-indent",
// )

lazy val CatsVersion = "2.9.0"
lazy val CatsEffectVersion = "3.5.1"
lazy val Fs2Version = "3.7.0"
lazy val MUnitVersion = "1.0.0-M6"

libraryDependencies ++= Seq(
  "org.scala-lang.modules" %% "scala-parser-combinators" % "2.1.0",
  "org.typelevel" %% "cats-core" % CatsVersion,
  "org.typelevel" %% "cats-kernel" % CatsVersion,
  "org.typelevel" %% "cats-free" % CatsVersion,
  "org.typelevel" %% "cats-effect" % CatsEffectVersion,
  "co.fs2" %% "fs2-core" % Fs2Version,
  "co.fs2" %% "fs2-io" % Fs2Version,
  "org.scalameta" %% "munit" % MUnitVersion % Test,
  "org.scalameta" %% "munit-scalacheck" % MUnitVersion % Test
  // "org.scalacheck" %% "scalacheck" % "1.14.1" % Test
)

// Here, `libraryDependencies` is a set of dependencies, and by using `+=`,
// we're adding the scala-parser-combinators dependency to the set of dependencies
// that sbt will go and fetch when it starts up.
// Now, in any Scala file, you can import classes, objects, etc., from
// scala-parser-combinators with a regular import.

// TIP: To find the "dependency" that you need to add to the
// `libraryDependencies` set, which in the above example looks like this:

// "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"

// You can use Scaladex, an index of all known published Scala libraries. There,
// after you find the library you want, you can just copy/paste the dependency
// information that you need into your build file. For example, on the
// scala/scala-parser-combinators Scaladex page,
// https://index.scala-lang.org/scala/scala-parser-combinators, you can copy/paste
// the sbt dependency from the sbt box on the right-hand side of the screen.

// IMPORTANT NOTE: while build files look _kind of_ like regular Scala, it's
// important to note that syntax in *.sbt files doesn't always behave like
// regular Scala. For example, notice in this build file that it's not required
// to put our settings into an enclosing object or class. Always remember that
// sbt is a bit different, semantically, than vanilla Scala.

// ============================================================================

// Most moderately interesting Scala projects don't make use of the very simple
// build file style (called "bare style") used in this build.sbt file. Most
// intermediate Scala projects make use of so-called "multi-project" builds. A
// multi-project build makes it possible to have different folders which sbt can
// be configured differently for. That is, you may wish to have different
// dependencies or different testing frameworks defined for different parts of
// your codebase. Multi-project builds make this possible.

// Here's a quick glimpse of what a multi-project build looks like for this
// build, with only one "subproject" defined, called `root`:

// lazy val root = (project in file(".")).
//   settings(
//     inThisBuild(List(
//       organization := "ch.epfl.scala",
//       scalaVersion := "2.13.3"
//     )),
//     name := "hello-world"
//   )

// To learn more about multi-project builds, head over to the official sbt
// documentation at http://www.scala-sbt.org/documentation.html
